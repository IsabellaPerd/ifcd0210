/*Consultas*/
SELECT * FROM sakila.film;
SELECT film_id FROM film WHERE rental_duration = 4;
SELECT title ORDER BY last_update;
SELECT actor_name FROM actor WHERE name_id=3333;
/*JOIN*/
CREATE VIEW actorName AS
SELECT first_name as nombre,
       last_name as apellido,
       actor_id as id
 FROM actor
   WHERE first_name = 'Regina';
/*Triggers*/
DELIMITER 
CREATE TRIGGER filmTrigger BEFORE INSERT ON film_category
FOR EACH ROW BEGIN 
INSERT INTO inventory(fiml_id) VALUE (NEW.film_id);
END

DELIMITER 
CREATE TRIGGER cityTrigger AFTER INSERT ON address
FOR EACH ROW BEGIN 
INSERT INTO address(city_id) VALUE (NEW.city_id);
END
/*Función*/
SELECT firts_name, UPPER(firts_name) FROM actor 

/*Procedimiento*/
DELIMITER //
CREATE PROCEDURE actorXfilm (IN id INT) 
BEGIN 
SELECT * FROM actor WHERE film_id=id;
END //
DELIMITER ;

   
