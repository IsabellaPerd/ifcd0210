import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class Calculadora {

	private JFrame frame;
	private JTextField txtDisplay;
	double firstnum;
	double secondnum;
	double resultado;
	String operations;
	String respuesta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculadora window = new Calculadora();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculadora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 287, 434);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txtDisplay = new JTextField();
		txtDisplay.setBounds(10, 11, 248, 37);
		txtDisplay.setHorizontalAlignment(SwingConstants.RIGHT);
		frame.getContentPane().add(txtDisplay);
		txtDisplay.setColumns(10);
		
		//-----------------Fila 1--------------------
		
		JButton btnBorrar = new JButton("←");
		btnBorrar.setBounds(10, 59, 60, 60);
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String backspace = null;
				 if (txtDisplay.getText().length() > 0){
					 StringBuilder strB = new StringBuilder (txtDisplay.getText());
					 strB.deleteCharAt(txtDisplay.getText().length() - 1);
					 backspace = strB.toString();
					 txtDisplay.setText(backspace);
				 }
				
			}
		});
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnBorrar);
		
		JButton btnResetear = new JButton("C");
		btnResetear.setBounds(75, 59, 60, 60);
		btnResetear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				txtDisplay.setText(null);

			}
		});
		btnResetear.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnResetear);
		
		JButton btnPorc = new JButton("%");
		btnPorc.setBounds(140, 59, 60, 60);
		btnPorc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnum = Double.parseDouble(txtDisplay.getText());
				txtDisplay.setText("");
				operations = "%";
			}
		});
		btnPorc.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnPorc);
		
		JButton btnSuma = new JButton("+");
		btnSuma.setBounds(203, 59, 60, 60);
		btnSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnum = Double.parseDouble(txtDisplay.getText());
				txtDisplay.setText("");
				operations = "+";
				
			}
		});
		btnSuma.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnSuma);
		
		//-----------------Fila 2--------------------
		
		JButton btn7 = new JButton("7");
		btn7.setBounds(10, 125, 60, 60);
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn7.getText();
				txtDisplay.setText(EnterNumber);
			}
		});
		
		btn7.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn7);
			
		JButton btn8 = new JButton("8");
		btn8.setBounds(75, 125, 60, 60);
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn8.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn8.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn8);
		
		JButton btn9 = new JButton("9");
		btn9.setBounds(140, 125, 60, 60);
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn9.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn9.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn9);
		
		JButton btnDiv = new JButton("/");
		btnDiv.setBounds(203, 259, 60, 60);
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnum = Double.parseDouble(txtDisplay.getText());
				txtDisplay.setText("");
				operations = "/";
			}
		});
		btnDiv.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnDiv);
		
		//-----------------Fila 3--------------------
		
		JButton btn4 = new JButton("4");
		btn4.setBounds(10, 193, 60, 60);
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn4.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn4.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn4);
		
		JButton btn5 = new JButton("5");
		btn5.setBounds(75, 193, 60, 60);
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn5.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn5.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn5);
		
		JButton btn6 = new JButton("6");
		btn6.setBounds(140, 193, 60, 60);
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn6.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn6.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn6);
		
		JButton btnMenos = new JButton("-");
		btnMenos.setBounds(203, 125, 60, 60);
		btnMenos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnum = Double.parseDouble(txtDisplay.getText());
				txtDisplay.setText("");
				operations = "-";
			}
		});
		btnMenos.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnMenos);
		
		//-----------------Fila 4--------------------
		
		JButton btn1 = new JButton("1");
		btn1.setBounds(10, 259, 60, 60);
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn1.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn1.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn1);
		
		JButton btn2 = new JButton("2");
		btn2.setBounds(75, 259, 60, 60);
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn2.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn2.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn2);
		
		JButton btn3 = new JButton("3");
		btn3.setBounds(140, 259, 60, 60);
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn3.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn3.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn3);
		
		JButton btnMult = new JButton("*");
		btnMult.setBounds(203, 193, 60, 60);
		btnMult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnum = Double.parseDouble(txtDisplay.getText());
				txtDisplay.setText("");
				operations = "*";
			}
		});
		btnMult.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnMult);
		
		//-----------------Fila 5--------------------
		
		JButton btn0 = new JButton("0");
		btn0.setBounds(10, 325, 60, 60);
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btn0.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btn0.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btn0);
		
		JButton btnPunto = new JButton(".");
		btnPunto.setBounds(75, 325, 60, 60);
		btnPunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = txtDisplay.getText() + btnPunto.getText();
				txtDisplay.setText(EnterNumber);

			}
		});
		btnPunto.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnPunto);
		
		JButton btnMM = new JButton("±");
		btnMM.setBounds(140, 325, 60, 60);
		btnMM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double ops = Double.parseDouble(String.valueOf(txtDisplay.getText()));
				ops = ops * (-1);
				txtDisplay.setText(String.valueOf(ops));
			}
		});
		btnMM.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnMM);
		
		JButton btnIgual = new JButton("=");
		btnIgual.setBounds(205, 325, 60, 60);
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String respuesta;	
			secondnum = Double.parseDouble(txtDisplay.getText());
			if (operations == "+");
			{
				resultado = firstnum + secondnum;
				respuesta = String.format("%.2f",resultado);
				txtDisplay.setText(respuesta);
			}
			 if (operations == "-")
			{
				resultado = firstnum / secondnum;
				respuesta = String.format("%.2f",resultado);
				txtDisplay.setText(respuesta);
			}
			else if (operations == "*")
			{
				resultado = firstnum * secondnum;
				respuesta = String.format("%.2f",resultado);
				txtDisplay.setText(respuesta); 
			}
			else if (operations == "/")
			{
				resultado = firstnum / secondnum;
				respuesta = String.format("%.2f",resultado);
				txtDisplay.setText(respuesta);
			}
			else if (operations == "%")
			{
				resultado = firstnum % secondnum;
				respuesta = String.format("%.2f",resultado);
				txtDisplay.setText(respuesta);
			}
				}
		});
		btnIgual.setFont(new Font("Tahoma", Font.BOLD, 18));
		frame.getContentPane().add(btnIgual);


	}
}
