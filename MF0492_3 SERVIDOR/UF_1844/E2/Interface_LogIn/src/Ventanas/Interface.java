package Ventanas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class Interface extends JFrame {
	private JPanel contentPane;
	private JTextField textFieldUser;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interface() {
		
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 586);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLogIn = new JButton("Log In");
		btnLogIn.setBackground(new Color(0, 51, 102));
		btnLogIn.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnLogIn.setForeground(new Color(255, 255, 255));
		btnLogIn.setBounds(275, 475, 89, 39);
		contentPane.add(btnLogIn);
		
		JLabel labelUser = new JLabel("");
		labelUser.setIcon(new ImageIcon("E:\\Curso_Paginas_Web\\Modulo 2\\UF_1844\\E2\\imagenes\\User.png"));
		labelUser.setBounds(75, 50, 238, 280);
		contentPane.add(labelUser);
		
		JLabel lblUsuario = new JLabel("Usuario :");
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblUsuario.setForeground(Color.WHITE);
		lblUsuario.setBounds(60, 386, 89, 28);
		contentPane.add(lblUsuario);
		
		JLabel lblPassword = new JLabel("Password : ");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(45, 425, 104, 29);
		contentPane.add(lblPassword);
		
		textFieldUser = new JTextField();
		textFieldUser.setBackground(new Color(0, 51, 102));
		textFieldUser.setForeground(new Color(255, 255, 255));
		textFieldUser.setFont(new Font("Tahoma", Font.BOLD, 14));
		textFieldUser.setBounds(159, 388, 205, 27);
		contentPane.add(textFieldUser);
		textFieldUser.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.BOLD, 14));
		passwordField.setForeground(new Color(255, 255, 255));
		passwordField.setBackground(new Color(0, 51, 102));
		passwordField.setBounds(159, 427, 205, 27);
		contentPane.add(passwordField);
		
		JLabel labelFondo = new JLabel("");
		labelFondo.setBackground(new Color(0, 0, 128));
		labelFondo.setFont(new Font("Tahoma", Font.BOLD, 14));
		labelFondo.setForeground(Color.WHITE);
		labelFondo.setBounds(10, 5, 399, 537);
		labelFondo.setHorizontalAlignment(SwingConstants.TRAILING);
		labelFondo.setIcon(new ImageIcon("E:\\Curso_Paginas_Web\\Modulo 2\\UF_1844\\E2\\imagenes\\Fondo.jpg"));
		contentPane.add(labelFondo);
		
	}
}
