package Pack_E2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class JFRm_E2 extends JFrame {

	private JPanel contentPane;
	private JTextField txtA�oActual;
	private JTextField txtA�oNac;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFRm_E2 frame = new JFRm_E2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFRm_E2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 240, 240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("A\u00F1o Actual");
		lblNewLabel.setBounds(10, 11, 52, 27);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("A\u00F1o de Nacimiento ");
		lblNewLabel_1.setBounds(10, 49, 92, 27);
		contentPane.add(lblNewLabel_1);
		
		txtA�oActual = new JTextField();
		txtA�oActual.setBounds(124, 14, 86, 20);
		contentPane.add(txtA�oActual);
		txtA�oActual.setColumns(10);
		
		txtA�oNac = new JTextField();
		txtA�oNac.setBounds(124, 52, 86, 20);
		contentPane.add(txtA�oNac);
		txtA�oNac.setColumns(10);
		
		JTextArea txtResultados = new JTextArea();
		txtResultados.setBounds(10, 106, 325, 89);
		contentPane.add(txtResultados);
		
		JButton btnProcesar = new JButton("Procesar");
		btnProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int A�oActual=Integer.parseInt(txtA�oActual.getText());
				int A�oNacimiento=Integer.parseInt(txtA�oNac.getText ());
				int edad;
				//Operaciones
				edad=A�oActual-A�oNacimiento;
				
				txtResultados.setText("TU EDAD ACTUAL ES :"+ edad);
				
			}
		});
		btnProcesar.setBounds(246, 30, 89, 23);
		contentPane.add(btnProcesar);
		

	}
}
