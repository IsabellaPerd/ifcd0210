import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;

public class Ventana2 extends JFrame {
	private JTextField txt_nombre;
	private JTextField txt_salario;
	private ButtonGroup botones=new ButtonGroup();

	public Ventana2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre : ");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(10, 25, 55, 22);
		getContentPane().add(lblNombre);
		
		txt_nombre = new JTextField();
		txt_nombre.setBackground(new Color(51, 102, 153));
		txt_nombre.setFont(new Font("Tahoma", Font.BOLD, 12));
		txt_nombre.setForeground(new Color(255, 255, 255));
		txt_nombre.setBounds(75, 26, 222, 20);
		getContentPane().add(txt_nombre);
		txt_nombre.setColumns(10);
		
		JLabel lblEdad = new JLabel("Edad :");
		lblEdad.setForeground(Color.WHITE);
		lblEdad.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEdad.setBounds(20, 81, 45, 14);
		getContentPane().add(lblEdad);
		
		final JComboBox cmb_edad = new JComboBox();
		cmb_edad.setForeground(new Color(255, 255, 255));
		cmb_edad.setFont(new Font("Tahoma", Font.BOLD, 12));
		cmb_edad.setBackground(new Color(51, 102, 153));
		cmb_edad.setModel(new DefaultComboBoxModel(new String[] {"15-25 a\u00F1os", "26-35 a\u00F1os", "36-45 a\u00F1os", "46-55 a\u00F1os", "m\u00E1s de 55 a\u00F1os"}));
		cmb_edad.setBounds(75, 78, 145, 20);
		getContentPane().add(cmb_edad);
		
		JLabel lblSexo = new JLabel("Sexo :");
		lblSexo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSexo.setForeground(Color.WHITE);
		lblSexo.setBounds(19, 146, 46, 14);
		getContentPane().add(lblSexo);
		
		final JRadioButton rdbtnMasculino = new JRadioButton("Masculino");
		rdbtnMasculino.setBackground(new Color(51, 102, 153));
		rdbtnMasculino.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnMasculino.setForeground(new Color(255, 255, 255));
		rdbtnMasculino.setBounds(75, 142, 109, 23);
		getContentPane().add(rdbtnMasculino);
		
		final JRadioButton rdbtnFemenino = new JRadioButton("Femenino");
		rdbtnFemenino.setBackground(new Color(51, 102, 153));
		rdbtnFemenino.setForeground(new Color(255, 255, 255));
		rdbtnFemenino.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnFemenino.setBounds(188, 142, 109, 23);
		getContentPane().add(rdbtnFemenino);
		
		botones.add(rdbtnMasculino);
		botones.add(rdbtnFemenino);
		
		JLabel lblSalario = new JLabel(" Salario :");
		lblSalario.setForeground(Color.WHITE);
		lblSalario.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSalario.setBounds(10, 206, 55, 14);
		getContentPane().add(lblSalario);
		
		txt_salario = new JTextField();
		txt_salario.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txt_salario.setForeground(new Color(255, 255, 255));
		txt_salario.setBackground(new Color(51, 102, 153));
		txt_salario.setBounds(75, 203, 109, 22);
		getContentPane().add(txt_salario);
		txt_salario.setColumns(10);
		
		JButton btnIngresar = new JButton("Ingresar");
		btnIngresar.setBackground(new Color(51, 102, 153));
		btnIngresar.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIngresar.setForeground(new Color(255, 255, 255));
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String mensaje;
				String mensaje1, sexo;
				if (rdbtnMasculino.isSelected()){
					sexo="Masculino";
					
				}
				else {
					if (rdbtnFemenino.isSelected()){
						sexo="Femenino";
					}
					else {
						sexo="desconocido";
					}
					
				}
				mensaje1=txt_nombre.getText()+" que tiene un rango de edad de "+cmb_edad.getSelectedItem()+" de sexo "+sexo+" y que tiene un salario de "+txt_salario.getText();
				System.out.println(mensaje1);
				
			}
		});
		btnIngresar.setBounds(312, 25, 99, 23);
		getContentPane().add(btnIngresar);
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.setBackground(new Color(51, 102, 153));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);		
			}
		});
		btnNewButton.setBounds(322, 202, 89, 23);
		getContentPane().add(btnNewButton);
		
		JLabel lbl_fondoVentana2 = new JLabel("New label");
		lbl_fondoVentana2.setIcon(new ImageIcon("E:\\Curso_Paginas_Web\\Modulo 2\\UF_1844\\E2\\InterfaceVentana\\img\\Fondo.jpg"));
		lbl_fondoVentana2.setBounds(0, 0, 434, 261);
		getContentPane().add(lbl_fondoVentana2);
	}
}
