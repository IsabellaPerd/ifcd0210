import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;

public class Ventana1 extends JFrame {

	private JPanel contentPane;
	public static JTextField textFieldUser;
	public static JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana1 frame = new Ventana1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 286, 460);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("User Name : ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(10, 303, 90, 14);
		contentPane.add(lblNewLabel);
		
		textFieldUser = new JTextField();
		textFieldUser.setFont(new Font("Tahoma", Font.BOLD, 12));
		textFieldUser.setForeground(new Color(255, 255, 255));
		textFieldUser.setBackground(new Color(51, 102, 153));
		textFieldUser.setBounds(110, 302, 150, 20);
		contentPane.add(textFieldUser);
		textFieldUser.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(20, 344, 80, 14);
		contentPane.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setForeground(new Color(255, 255, 255));
		passwordField.setFont(new Font("Tahoma", Font.BOLD, 12));
		passwordField.setBackground(new Color(51, 102, 153));
		passwordField.setBounds(110, 343, 150, 20);
		contentPane.add(passwordField);
		
		final DatosUsuario data=new DatosUsuario();
		final Ventana2 window2=new Ventana2 ();
		
		JButton btnLogIn = new JButton("Log In");
		btnLogIn.setForeground(new Color(255, 255, 255));
		btnLogIn.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnLogIn.setBackground(new Color(51, 102, 153));
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (data.probarPass()==1) {
					JOptionPane.showMessageDialog(null, "Bienvenido al Sistema");
					window2.setVisible(true);
					dispose();
				}
				else {
					JOptionPane.showMessageDialog(null, "Error. Usuario o contraseņa incorrectos");
				}
			}
		});
		btnLogIn.setBounds(143, 387, 89, 23);
		contentPane.add(btnLogIn);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBackground(new Color(51, 102, 153));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(44, 387, 89, 23);
		contentPane.add(btnSalir);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("E:\\Curso_Paginas_Web\\Modulo 2\\UF_1844\\E2\\InterfaceVentana\\img\\User.png"));
		lblNewLabel_1.setBounds(0, 11, 232, 268);
		contentPane.add(lblNewLabel_1);
		
		JLabel Fondo = new JLabel("New label");
		Fondo.setIcon(new ImageIcon("E:\\Curso_Paginas_Web\\Modulo 2\\UF_1844\\E2\\InterfaceVentana\\img\\Fondo.jpg"));
		Fondo.setBounds(0, 0, 270, 421);
		contentPane.add(Fondo);
	}
}
