package Serv;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servl
 */
@WebServlet("/Servl")
public class Servl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		ServletOutputStream out = response.getOutputStream();
		response.setContentType("text/html");
		out.println("<html><head><title>Basic Form Processor Output</head>");
		out.println("<head></head>");
		out.println("<body>");
		out.println("<h1>Here is your Form Data</h1>");
		
		//Opci�n elgida en el cuadro combinado (tratamiento)
		out.println("<br>Your title is "+ request.getParameter("title"));
		
		//Par�metros nombre, ciudad, pa�s y tel�fono (individuales text)
		out.println("<br>Your name is "+ request.getParameter("name"));
		out.println("<br>Your city is "+ request.getParameter("city"));
		out.println("<br>Your country is "+ request.getParameter("country"));
		out.println("<br>Your tel is "+ request.getParameter("tel"));
		
		//Extraer Data del parametro Checkbox (checkbox intereses)
		String[] interests = request.getParameterValues("interests");
		if(interests!=null) {
			out.println("</br>Your interest include<ul> ");
				for (int i=0; i<interests.length; i++) {
					out.println("<li>" + interests[i]);
				}
				out.println("</ul>");
		}
		
		else {
			out.println("<p>No tiene aficiones...</p>");
			
		}
		
		//Opci�n elegida (edad radio)
		out.println("<br>Your age is " + request.getParameter("age"));
		out.println("</body></html>");
		}
	

	
		
	}


