
package pqsl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pqsl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Area_QNAME = new QName("http://pqsl/", "Area");
    private final static QName _HelloResponse_QNAME = new QName("http://pqsl/", "helloResponse");
    private final static QName _AreaResponse_QNAME = new QName("http://pqsl/", "AreaResponse");
    private final static QName _Hello_QNAME = new QName("http://pqsl/", "hello");
    private final static QName _VolumenResponse_QNAME = new QName("http://pqsl/", "VolumenResponse");
    private final static QName _Volumen_QNAME = new QName("http://pqsl/", "Volumen");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pqsl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Area }
     * 
     */
    public Area createArea() {
        return new Area();
    }

    /**
     * Create an instance of {@link AreaResponse }
     * 
     */
    public AreaResponse createAreaResponse() {
        return new AreaResponse();
    }

    /**
     * Create an instance of {@link HelloResponse }
     * 
     */
    public HelloResponse createHelloResponse() {
        return new HelloResponse();
    }

    /**
     * Create an instance of {@link Hello }
     * 
     */
    public Hello createHello() {
        return new Hello();
    }

    /**
     * Create an instance of {@link Volumen }
     * 
     */
    public Volumen createVolumen() {
        return new Volumen();
    }

    /**
     * Create an instance of {@link VolumenResponse }
     * 
     */
    public VolumenResponse createVolumenResponse() {
        return new VolumenResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Area }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pqsl/", name = "Area")
    public JAXBElement<Area> createArea(Area value) {
        return new JAXBElement<Area>(_Area_QNAME, Area.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HelloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pqsl/", name = "helloResponse")
    public JAXBElement<HelloResponse> createHelloResponse(HelloResponse value) {
        return new JAXBElement<HelloResponse>(_HelloResponse_QNAME, HelloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AreaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pqsl/", name = "AreaResponse")
    public JAXBElement<AreaResponse> createAreaResponse(AreaResponse value) {
        return new JAXBElement<AreaResponse>(_AreaResponse_QNAME, AreaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Hello }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pqsl/", name = "hello")
    public JAXBElement<Hello> createHello(Hello value) {
        return new JAXBElement<Hello>(_Hello_QNAME, Hello.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VolumenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pqsl/", name = "VolumenResponse")
    public JAXBElement<VolumenResponse> createVolumenResponse(VolumenResponse value) {
        return new JAXBElement<VolumenResponse>(_VolumenResponse_QNAME, VolumenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Volumen }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pqsl/", name = "Volumen")
    public JAXBElement<Volumen> createVolumen(Volumen value) {
        return new JAXBElement<Volumen>(_Volumen_QNAME, Volumen.class, null, value);
    }

}
