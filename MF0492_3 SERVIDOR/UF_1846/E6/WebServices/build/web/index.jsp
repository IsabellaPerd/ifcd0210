<%-- 
    Document   : index
    Created on : 23-may-2019, 18:14:47
    Author     : pc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../src/java/Style.css" rel="stylesheet" type="text/css"/>
        <title>Web Services</title>
    </head>
    <body>
        <h1>Servicios Web</h1>     
        <h2>Volumen Circunferencia</h2>
        <p>Ingrese radio para calcular el volumen del circulo.</p>
        <form  method="POST">
            <input type="text" name="cajaRadio" value="" />
            <input type="submit" value="Calcular"/>
        </form>
    <%-- start web service invocation --%><hr/>
    <%
    try {
	pqsl.ServiceWSDL_Service service = new pqsl.ServiceWSDL_Service();
	pqsl.ServiceWSDL port = service.getServiceWSDLPort();
	 // TODO initialize WS operation arguments here
	java.lang.Double radio = Double.valueOf("cajaRadio");
	// TODO process result here
	java.lang.Double result = port.volumen(radio);
        if (radio <=0)     
            out.println("El radio no puede ser igual o menor que cero");
        else
            out.println("El volumen de la esfera es = "+result);
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }
    %>
    <%-- end web service invocation --%><hr/>

    
   <h2>Area de una Circunferencia</h2>
    <p>Ingrese radio para calcular el area del circulo.</p>
    <form  method="POST">
            <input type="text" name="cajaRadio2" value="" />
            <input type="submit" value="Calcular"/>
    </form>
        
     <%-- start web service invocation --%><hr/>
    <%
    try {
	pqsl.ServiceWSDL_Service service = new pqsl.ServiceWSDL_Service();
	pqsl.ServiceWSDL port = service.getServiceWSDLPort();
	 // TODO initialize WS operation arguments here
	java.lang.Double radio2 = Double.valueOf("cajaRadio2");
	// TODO process result here
	java.lang.Double result = port.area(radio2);
	if (radio2 <=0)
            out.println("El radio no puede ser igual o menor que cero");
        else
            out.println("El area de la esfera es = "+result);
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }
    %>
    <%-- end web service invocation --%><hr/>



    </body>
</html>
