/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pqsl;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author pc
 */
@WebService(serviceName = "ServiceWSDL")
public class ServiceWSDL {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
        /**
     * Web service operation
     * @param radio
     * @return 
     */
    @WebMethod(operationName = "Volumen")
    public Double Volumen(@WebParam(name = "radio") Double radio) {
        //TODO write your implementation code here:
        return (4.0/3.0) * (Math.PI) * Math.pow(radio,3);
    }

    /**
     * Web service operation
     * @param radio
     * @return 
     */
    @WebMethod(operationName = "Area")
    public Double Area(@WebParam(name = "radio2") Double radio) {
        //TODO write your implementation code here:
        return  (Math.PI)* Math.pow(radio,2);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "Addition")
    public String Addition(@WebParam(name = "i") float i, @WebParam(name = "j") float j) {
        float res = i+j;
        return (Float.toString(res));
    }
    
    
    
}
